# `@joaopalmeiro/biome-react-vike-config`

My personal Biome config for [React](https://react.dev/) + [Vike](https://vike.dev/) projects.

- [Source code](https://gitlab.com/joaommpalmeiro/biome-react-vike-config)
- [npm package](https://www.npmjs.com/package/@joaopalmeiro/biome-react-vike-config)
- [Licenses](https://licenses.dev/npm/%40joaopalmeiro%2Fbiome-react-vike-config/0.1.0)
- [Package Phobia](https://packagephobia.com/result?p=@joaopalmeiro/biome-react-vike-config@0.1.0)
- [npm trends](https://npmtrends.com/@joaopalmeiro/biome-react-vike-config)

## Getting Started

### Installation

```bash
npm install --save-dev @joaopalmeiro/biome-react-vike-config
```

or

```bash
yarn add --dev @joaopalmeiro/biome-react-vike-config
```

or

```bash
pnpm add --save-dev @joaopalmeiro/biome-react-vike-config
```

or

```bash
bun add --dev @joaopalmeiro/biome-react-vike-config
```

### Usage

To use this configuration, create a `biome.json`/`biome.jsonc` file and add the following snippet:

```json
{
  "extends": ["@joaopalmeiro/biome-react-vike-config"]
}
```

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run lint
```

```bash
npm run format
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the version in the `Licenses` and `Package Phobia` links at the top.

```bash
echo "v$(npm pkg get version | tr -d \")" | pbcopy
```

- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/biome-react-vike-config/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/@joaopalmeiro/biome-react-vike-config).
